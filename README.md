# elastic_test

Dummy golang app to search dummy info from an ElasticSearch instance.

## How to run

```
# cat > .env <<-END
ELASTICSEARCH_URL=https://<your_elastic_stack_url>:<port>
END
# export $(cat .env | xargs)
# go run main.go
```