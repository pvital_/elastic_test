// Copyright (c) 2021 Paulo Vital
// License: MIT

// Entry point for the app.
package main

import (
	"log"

	"github.com/elastic/go-elasticsearch/v8"
)

func main() {
	log.Println("#### Elastic Test ####")
	log.Printf("#### Using go-elasticsearch pkg version: %s\n", elasticsearch.Version)

	// Initialize a client with the default settings.
	//
	// An `ELASTICSEARCH_URL` environment variable will be used when exported.
	//
	es, err := elasticsearch.NewDefaultClient()
	if err != nil {
		log.Fatalf("Error creating the client: %s", err)
	}

	// Print ElasticSearch info
    log.Println(es.Info())

	log.Println("########")
}
